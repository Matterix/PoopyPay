package dk.lundudvikling.poopypay.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import dk.lundudvikling.poopypay.Handlers.FormatHandler;
import dk.lundudvikling.poopypay.Handlers.FragmentHandler;
import dk.lundudvikling.poopypay.Models.DBItem;
import dk.lundudvikling.poopypay.R;
import dk.lundudvikling.poopypay.utils.RecyclerItemClickListener;
import es.dmoral.toasty.Toasty;


public class HistoryFragment extends BaseFragment {


    private FragmentHandler fragmentHandler;
    private ArrayList<DBItem> mAllItems;
    private HistoryItemAdapter mAdapter;

    @BindView(R.id.rv_history_items) RecyclerView mRecyclerView;
    @BindView(R.id.db_total_time_spent) TextView dbTimeSpent ;
    @BindView(R.id.db_total_earned) TextView dbTotalEarned;
    @BindView(R.id.db_total_visits) TextView dbTotalVisits;

    private Button dialogDeleteItem;
    private TextView dialogHourlyWage;
    private TextView dialogTimeSpent;
    private TextView dialogMoneyEarned;
    private TextView dialogTitle;
    private TextView dialogDate;
    private AlertDialog itemDialog;
    private int itemToDelete;
    private AlertDialog.Builder deleteDialogBuilder;
    private AlertDialog deleteDialog;
    private float totalMoneyEarned;
    private float totalTimeSpent;

    @BindString(R.string.history_amount_of_visit) String amountOfVisitString;
    @BindString(R.string.history_delete_visit) String deleteVisitString;
    @BindString(R.string.history_delete_dialog_title) String deleteDialogTitleString;
    @BindString(R.string.history_visit_deleted) String visitDeletedString;
    @BindString(R.string.history_total_earned_no_visit) String totalEarnedNoVisitString;
    @BindString(R.string.history_time_spent_no_visit) String totalTimeSpentNoVisitString;
    @BindString(R.string.string_no) String stringNo;
    @BindString(R.string.string_yes) String stringYes;
    @BindString(R.string.item_time_spent) String totalTimeSpentString;
    @BindString(R.string.item_total_earned) String totalEarnedString;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        fragmentHandler = new FragmentHandler(getMainActivity());
        ButterKnife.bind(this, rootView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAllItems = new ArrayList<>(DBItem.listAll(DBItem.class));
        dbTotalVisits.setText(amountOfVisitString + mAllItems.size());
        for (DBItem item : mAllItems){
            totalMoneyEarned += item.totalEarned;
            totalTimeSpent += Float.valueOf(item.timeSpent);
        }
        Collections.reverse(mAllItems);
        mAdapter = new HistoryItemAdapter(mAllItems);
        dbTotalEarned.setText(totalEarnedString + FormatHandler.getFormatHandler().getFormattedCurrencyString(totalMoneyEarned));
        dbTimeSpent.setText(totalTimeSpentString + FormatHandler.getFormatHandler().getFormattedTimeString(totalTimeSpent));
        setupAdapter();
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
                View mView = getLayoutInflater().inflate(R.layout.item_dialog, null);
                dialogDeleteItem = mView.findViewById(R.id.dialog_delete_item);
                dialogHourlyWage = mView.findViewById(R.id.dialog_hourly_wage_show);
                dialogTimeSpent = mView.findViewById(R.id.dialog_time_spent_show);
                dialogMoneyEarned = mView.findViewById(R.id.dialog_money_earned_show);
                dialogTitle = mView.findViewById(R.id.dialog_title);
                dialogDate = mView.findViewById(R.id.dialog_datetime);

                dialogTitle.setText(mAllItems.get(position).note);
                dialogDate.setText(mAllItems.get(position).dateTime);
                dialogHourlyWage.setText(FormatHandler.getFormatHandler().getWeeklyWageString(Float.valueOf(mAllItems.get(position).hourlyWage)));
                dialogTimeSpent.setText(FormatHandler.getFormatHandler().getFormattedTimeString(Float.valueOf(mAllItems.get(position).timeSpent)));
                dialogMoneyEarned.setText(FormatHandler.getFormatHandler().getFormattedCurrencyString(Float.valueOf(mAllItems.get(position).totalEarned)));

                dialogDeleteItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteItemDialog();
                    }
                });
                itemToDelete = position;
                mBuilder.setView(mView);
                itemDialog = mBuilder.create();
                itemDialog.show();
            }
            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));
    }

    public void deleteItemDialog(){
        deleteDialogBuilder = new AlertDialog.Builder(getContext());
        deleteDialogBuilder.setMessage(deleteVisitString)
                .setTitle(deleteDialogTitleString);
        deleteDialogBuilder.setNegativeButton(stringNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        deleteDialogBuilder.setPositiveButton(stringYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                long idDelete = mAllItems.get(itemToDelete).getId();
                mAllItems.remove(itemToDelete);
                DBItem item = DBItem.findById(DBItem.class, idDelete);
                totalMoneyEarned = totalMoneyEarned - item.totalEarned;
                totalTimeSpent = totalTimeSpent - Float.valueOf(item.timeSpent);
                item.delete();
                if (mAllItems.size() <= 0){
                    dbTotalEarned.setText(totalEarnedNoVisitString);
                    dbTimeSpent.setText(totalTimeSpentNoVisitString);
                }else{
                    dbTotalEarned.setText(totalEarnedString + FormatHandler.getFormatHandler().getFormattedCurrencyString(totalMoneyEarned));
                    dbTimeSpent.setText(totalTimeSpentString + FormatHandler.getFormatHandler().getFormattedTimeString(totalTimeSpent));
                }
                dbTotalVisits.setText(amountOfVisitString + mAllItems.size());
                mAdapter.notifyDataSetChanged();
                Toasty.info(getContext(), visitDeletedString, Toast.LENGTH_SHORT).show();
                itemDialog.dismiss();
            }
        });
        deleteDialog = deleteDialogBuilder.create();
        deleteDialog.show();

    }

    private class HistoryItemHolder extends RecyclerView.ViewHolder {
        private TextView tvHistoryItemTitle;
        private TextView tvHistoryItemDate;

        public HistoryItemHolder(View itemView) {
            super(itemView);
            tvHistoryItemTitle = itemView.findViewById(R.id.tv_history_item_title);
            tvHistoryItemDate = itemView.findViewById(R.id.tv_history_item_date);
        }
    }

    private class HistoryItemAdapter extends RecyclerView.Adapter<HistoryItemHolder> {
        private ArrayList<DBItem> allItems;
        public HistoryItemAdapter(ArrayList<DBItem> historyItems) {
            allItems = historyItems;
        }

        @Override
        public HistoryItemHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(R.layout.cv_history_item, viewGroup, false);
            return new HistoryItemHolder(view);
        }
        @Override
        public void onBindViewHolder(HistoryItemHolder historyItemHolder, int position) {
            DBItem item = HistoryFragment.this.mAllItems.get(position);
            historyItemHolder.tvHistoryItemTitle.setText(item.getNote());
            historyItemHolder.tvHistoryItemDate.setText(item.getDateTime());
        }

        @Override
        public int getItemCount() {
            return mAllItems.size();
        }
    }

    private void setupAdapter() {
        if (isAdded()) {
            mRecyclerView.setAdapter(mAdapter);
        }
    }

}