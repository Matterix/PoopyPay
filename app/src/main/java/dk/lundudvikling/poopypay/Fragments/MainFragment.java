package dk.lundudvikling.poopypay.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.lundudvikling.poopypay.Handlers.FormatHandler;
import dk.lundudvikling.poopypay.Handlers.FragmentHandler;
import dk.lundudvikling.poopypay.Models.DBItem;
import dk.lundudvikling.poopypay.R;
import dk.lundudvikling.poopypay.Utilities.Stopwatch;
import es.dmoral.toasty.Toasty;

import static android.content.Context.MODE_PRIVATE;


public class MainFragment extends BaseFragment {

    @BindView(R.id.tv_hourly_pay) TextView hourlyPay;
    @BindView(R.id.btn_stopwatch_timer) Button stopwatchTimer;
    @BindView(R.id.display_timer) TextView timerDisplay;
    @BindView(R.id.display_pay) TextView payDisplay;
    @BindString(R.string.main_start_timer) String startTimerString;
    @BindString(R.string.main_stop_timer) String stopTimerString;
    @BindString(R.string.main_cancel_visit) String cancelVisitString;
    @BindString(R.string.main_cancel_visit_check) String cancelVisitCheckString;
    @BindString(R.string.main_visit_cancelled) String visitCancelledString;
    @BindString(R.string.string_yes) String yesString;
    @BindString(R.string.string_no) String noString;
    @BindString(R.string.main_unnamed_visit) String unnamedVisitString;
    @BindView(R.id.layout_save_visit) LinearLayout layoutSaveVisit;
    @BindView(R.id.et_visit_note) EditText etVisitNote;
    @BindView(R.id.tv_username) TextView tvUsername;

    private Stopwatch stopwatch;
    private Thread timerThread;
    private Thread payThread;
    private FragmentHandler fragmentHandler;
    private float totalMoneyEarned;
    private String SHARED_PREF = "userPref";

    private SimpleDateFormat sdf;
    private Calendar cal;
    private String timeStamp;
    private Long timeSpentMs;
    private String currency;
    private String username;
    private Float income;

    private AdView mAdView;

    // TODO: 23-01-2018 Refaktor!

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        fragmentHandler = new FragmentHandler(getMainActivity());
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // TODO: 23-07-2018 Tilføj mulighed for flere typer currency i fremtiden!
        currency = "$";


        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        SharedPreferences prefs = getContext().getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        username = prefs.getString("username", null);
        income = prefs.getFloat("income", 0.0f);
        String hourlyPayString = String.valueOf(income) + " " + currency;

        hourlyPay.setText(hourlyPayString);
        tvUsername.setText(username);

        stopwatchTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stopwatchTimer.getText().toString().equals(startTimerString)) {
                    startTimerLogic();
                } else if (stopwatchTimer.getText().toString().equals(stopTimerString)) {
                    stopTimerLogic();
                }
            }
        });
    }

    public void startTimerLogic() {
        getTimeStamp();
        final float moneyPrHour, moneyPrMS;
        String displayString = "0" + " " + currency;

        stopwatchTimer.setText(stopTimerString);
        stopwatchTimer.setBackgroundColor(getResources().getColor(R.color.colorRed));
        layoutSaveVisit.setVisibility(View.INVISIBLE);
        stopwatch = new Stopwatch();
        moneyPrHour = income;
        moneyPrMS = (moneyPrHour / 60) / (60) / (1000);
        timerDisplay.setText(String.valueOf(0));

        payDisplay.setText(displayString);

        timerThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(33);
                        getMainActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                timerDisplay.setText(String.valueOf(FormatHandler.getFormatHandler().getFormattedTimeString(stopwatch.getElapsedTime().getElapsedRealtimeMillis())));
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    Log.i("THREADTAG", "timerThread crashed: " + e.getMessage());
                }
            }
        };

        payThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(99);
                        getMainActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                totalMoneyEarned = stopwatch.getElapsedTime().getElapsedRealtimeMillis() * moneyPrMS;
                                payDisplay.setText(FormatHandler.getFormatHandler().getFormattedCurrencyString(totalMoneyEarned));
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    Log.i("THREADTAG", "payThread crashed: " + e.getMessage());
                }
            }
        };
        timerThread.start();
        payThread.start();
    }

    public void stopTimerLogic() {
        stopwatchTimer.setBackgroundColor(getResources().getColor(R.color.colorGreen));
        timerDisplay.setText(String.valueOf(FormatHandler.getFormatHandler().getFormattedTimeString(stopwatch.getElapsedTime().getElapsedRealtimeMillis())));
        stopwatchTimer.setText(startTimerString);
        layoutSaveVisit.setVisibility(View.VISIBLE);
        timerThread.interrupt();
        payThread.interrupt();
        timeSpentMs = stopwatch.getElapsedTime().getElapsedRealtimeMillis();

    }

    @OnClick(R.id.btn_save_visit)
    void onSaveVisitClicked() {
        try {
            dbLogicHandler();
            fragmentHandler.startTransactionAnimated(new HistoryFragment(),
                    R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_discard_visit)
    void onDiscardVisitClicked() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext());
        builder.setTitle(cancelVisitString);
        builder.setMessage(cancelVisitCheckString);
        builder.setPositiveButton(yesString, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                payDisplay.setText("0");
                timerDisplay.setText("0");
                layoutSaveVisit.setVisibility(View.INVISIBLE);
                Toasty.error(getContext(), visitCancelledString, Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton(noString, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();
    }

    public void getTimeStamp() {
        sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
        cal = Calendar.getInstance();
        timeStamp = sdf.format(cal.getTime());
    }

    public void dbLogicHandler() throws ParseException {
        String note = etVisitNote.getText().toString();
        if (note.equals("")) {
            note = unnamedVisitString;
        }
        DBItem item = new DBItem(timeStamp, String.valueOf(income), String.valueOf(timeSpentMs), totalMoneyEarned, note);
        item.save();
    }
}
