package dk.lundudvikling.poopypay.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.lundudvikling.poopypay.Handlers.FragmentHandler;
import dk.lundudvikling.poopypay.MainActivity;
import dk.lundudvikling.poopypay.R;

import android.content.SharedPreferences;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import static android.content.Context.MODE_PRIVATE;


public class SplashScreenFragment extends BaseFragment {

    private FragmentHandler fragmentHandler;
    private String SHARED_PREF = "userPref";
    @BindView(R.id.splash_et_username) EditText etUsername;
    @BindView(R.id.splash_et_income) EditText etIncome;
    @BindView(R.id.splash_btn_new_user) Button btnNewUser;
    @BindView(R.id.splash_tv_title) TextView tvTitle;

    // TODO: 31-01-2018 Bedre design på denne side!
    // TODO: 23-07-2018 Fjern fabDrawer på den her side! Skal ikke være muligt at gå videre uden "login"

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash_screen, container, false);
        fragmentHandler = new FragmentHandler(getMainActivity());
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.splash_btn_new_user) void onNewUserClicked(){
        // TODO: 31-01-2018 Validering her
        SharedPreferences.Editor editor = getContext().getSharedPreferences(SHARED_PREF, MODE_PRIVATE).edit();
        editor.putString("username", etUsername.getText().toString());
        editor.putFloat("income", Float.valueOf(etIncome.getText().toString()));
        editor.apply();
        fragmentHandler.startTransactionAnimatedNoBackstack(new MainFragment(),
                R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
    }
}
