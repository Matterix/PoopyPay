package dk.lundudvikling.poopypay.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.lundudvikling.poopypay.Handlers.FragmentHandler;
import dk.lundudvikling.poopypay.R;
import es.dmoral.toasty.Toasty;

import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends BaseFragment {

    private String SHARED_PREF = "userPref";
    private String username;
    private Float income;

    private FragmentHandler fragmentHandler;

    @BindView(R.id.et_profile_name) EditText etProfileName;
    @BindView(R.id.et_profile_income) EditText etProfileIncome;
    @BindString(R.string.profile_changes_saved) String changesSavedString;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        fragmentHandler = new FragmentHandler(getMainActivity());
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getSharedPreferences();

    }

    @OnClick(R.id.btn_save_profile) void onSaveProfileClicked(){
        setSharedPreferences();
        Toasty.success(getContext(), changesSavedString, Toast.LENGTH_SHORT).show();
        fragmentHandler.startTransactionAnimated(new MainFragment(),
                R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
    }

    public void setSharedPreferences(){
        SharedPreferences.Editor editor = getContext().getSharedPreferences(SHARED_PREF, MODE_PRIVATE).edit();
        username = etProfileName.getText().toString();
        income = Float.valueOf(etProfileIncome.getText().toString());
        editor.putString("username", username);
        editor.putFloat("income", income);
        editor.apply();
    }


    public void getSharedPreferences(){
        SharedPreferences prefs = getContext().getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        username = prefs.getString("username",null);
        income = prefs.getFloat("income", 0.0f);
        etProfileName.setText(username);
        etProfileIncome.setText(String.valueOf(income));
    }
}
