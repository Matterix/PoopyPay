package dk.lundudvikling.poopypay;

import android.animation.ValueAnimator;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;


import com.google.android.gms.ads.MobileAds;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import dk.lundudvikling.poopypay.Fragments.HistoryFragment;
import dk.lundudvikling.poopypay.Fragments.MainFragment;
import dk.lundudvikling.poopypay.Fragments.ProfileFragment;
import dk.lundudvikling.poopypay.Fragments.SplashScreenFragment;
import dk.lundudvikling.poopypay.Handlers.FragmentHandler;
import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FragmentHandler fragmentHandler;
    private boolean isDrawerOpen;

    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.fabDrawer) FloatingActionButton mFabDrawer;
    @BindView(R.id.mainView) FrameLayout mMainView;
    @BindView(R.id.nav_view) NavigationView navigationView;

    @BindString(R.string.ad_mob_test_id) String adMobId;
    private String SHARED_PREF = "userPref";
    // TODO: 31-01-2018 Flyt shared pref ud i basefrag

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, adMobId);
        ButterKnife.bind(this);
        initializeNavigationDrawer();
        initializeButtons();
        setUpToasty();
        mFabDrawer.setColorFilter(getResources().getColor(R.color.colorPrimaryDark));

        fragmentHandler = new FragmentHandler(this);
        SharedPreferences prefs = getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        String username = prefs.getString("username",null);

        if (username == null){
            fragmentHandler.startTransactionNoBackStack(new SplashScreenFragment());
        }else{
            fragmentHandler.startTransactionNoBackStack(new MainFragment());
        }
    }

    private void initializeButtons() {
        mFabDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.START);
            }
        });
    }
    public void setUpToasty(){
        Toasty.Config.getInstance().apply();
    }

    private void initializeNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(this);
        mDrawerLayout.addDrawerListener(new ActionBarDrawerToggle(this, mDrawerLayout, null, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                isDrawerOpen = false;
                supportInvalidateOptionsMenu();
            }
            public void onDrawerOpened(View drawerView) {
                isDrawerOpen = true;
                supportInvalidateOptionsMenu();
            }
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mMainView.setTranslationX(slideOffset * drawerView.getWidth());
                mDrawerLayout.bringChildToFront(drawerView);
                mDrawerLayout.requestLayout();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mMainView.setZ(2);
                    mFabDrawer.setZ(1);
                }
                drawerAnimation(slideOffset);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.nav_history:
                fragmentHandler.startTransactionWithBackStack(new HistoryFragment());
                break;
            case R.id.nav_main:
                fragmentHandler.startTransactionWithBackStack(new MainFragment());
                break;
            case R.id.nav_profile:
                fragmentHandler.startTransactionWithBackStack(new ProfileFragment());
                break;
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void drawerAnimation(final float slideOffset) {
        final DrawerArrowDrawable drawable = new DrawerArrowDrawable(this);
        mFabDrawer.setImageDrawable(drawable);

        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                drawable.setProgress(slideOffset);
            }
        });
        if (isDrawerOpen) {
            animator.start();
        } else {
            animator.reverse();
        }
    }
}
