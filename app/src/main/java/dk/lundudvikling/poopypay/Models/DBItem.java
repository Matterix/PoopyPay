package dk.lundudvikling.poopypay.Models;


import com.orm.SugarRecord;

public class DBItem extends SugarRecord<DBItem> {

    public String dateTime;
    public String timeSpent;
    public float totalEarned;
    public String hourlyWage;
    public String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(String hourlyWage) {
        this.hourlyWage = hourlyWage;
    }

    public DBItem(String dateTime, String hourlyWage,  String timeSpent, float totalEarned, String note) {
        this.dateTime = dateTime;
        this.timeSpent = timeSpent;
        this.totalEarned = totalEarned;
        this.hourlyWage = hourlyWage;
        this.note = note;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(String timeSpent) {
        this.timeSpent = timeSpent;
    }

    public float getTotalEarned() {
        return totalEarned;
    }

    public void setTotalEarned(float totalEarned) {
        this.totalEarned = totalEarned;
    }

    public DBItem(){

    }
}