package dk.lundudvikling.poopypay.Handlers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.List;

import dk.lundudvikling.poopypay.Fragments.BaseFragment;
import dk.lundudvikling.poopypay.MainActivity;
import dk.lundudvikling.poopypay.R;


public class FragmentHandler {

    private FragmentManager fm;

    public FragmentHandler(MainActivity activity) {
        fm = activity.getSupportFragmentManager();
    }

    public void startTransactionNoAnimation(BaseFragment fragment) {
        fm.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
    }

    public void startTransactionAnimated(BaseFragment fragment, int startEnter, int startExit, int endEnter, int endExit) {
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setCustomAnimations(startEnter, startExit, endEnter, endExit);
        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void startTransactionAnimatedNoBackstack(BaseFragment fragment, int startEnter, int startExit, int endEnter, int endExit) {
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setCustomAnimations(startEnter, startExit, endEnter, endExit);
        transaction.replace(R.id.content_frame, fragment);
        transaction.commit();
    }

    public void startTransactionWithBackStack(BaseFragment fragment){
        fm.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
    }

    public void startTransactionNoBackStack(BaseFragment fragment) {
        fm.beginTransaction().replace(R.id.content_frame, fragment).commitAllowingStateLoss();
    }

    public void popBackStack() {
        fm.popBackStack();
    }

    public List<Fragment> getFragments() {
        return fm.getFragments();
    }

    public int getBackStackEntryCount() {
        return fm.getBackStackEntryCount();
    }
}
