package dk.lundudvikling.poopypay.Handlers;

public class FormatHandler {

    private static FormatHandler formatHandler;

    private FormatHandler() {
    }

    public static FormatHandler getFormatHandler() {
        if (formatHandler == null)
            formatHandler = new FormatHandler();
        return formatHandler;
    }

    public String getFormattedTimeString(double timeSpent) {
        // TODO: 30-01-2018 Minutter fungerer ikke - viser 60!
        double seconds = timeSpent / 1000.0;
        double minutes = Math.floor(seconds / 60);
        double hours = Math.floor(minutes / 60);
        // Næste skridt er dage - seconds < 86400

        if (seconds < 1.0) {
            return String.format("%.0f ms", seconds * 1000);
        }
        if (seconds < 60){
            return String.format("%.2f s", seconds);
        }
        if (seconds < 3600){
            return String.format("%.0f m", minutes) + " " + String.format("%.0f s", seconds % 60);
        }
        else{
            return String.format("%.0f h", hours) + " " + String.format("%.0f m", minutes % 60) + " " + String.format("%.0f s", seconds % 60);
        }
    }

    public String getFormattedCurrencyString(double totalMoney){
        return String.format("%.2f $", totalMoney );
    }

    public String getWeeklyWageString(double weeklyWage){
        return String.format("%.0f $", weeklyWage);
    }
}
