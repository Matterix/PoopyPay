package dk.lundudvikling.poopypay.Utilities;

import android.os.SystemClock;

import dk.lundudvikling.poopypay.Handlers.FormatHandler;


public class Stopwatch {
    private long startRealtimeMillis;

    public static class ElapsedTime {
        private final long elapsedRealtimeMillis;


        public ElapsedTime(Stopwatch stopwatch) {
            elapsedRealtimeMillis = SystemClock.elapsedRealtime() - stopwatch.startRealtimeMillis;
        }

        public long getElapsedRealtimeMillis() {
            return elapsedRealtimeMillis;
        }
    }

    public Stopwatch() {
        reset();
    }

    public void reset() {
        startRealtimeMillis = SystemClock.elapsedRealtime();
    }

    public ElapsedTime getElapsedTime() {
        return new ElapsedTime(this);
    }

}
